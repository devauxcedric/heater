A small C programm I used at university to keep myself warm during cold winter days :)

# Usage
## Installation
``` 
sudo apt install lm-sensors
make install
```

## Launching
```
./chauffage
```

## Removal
```
sudo apt remove lm-sensors # maybe skip this
make clean
```

# FAQ
## Is it a good heater
Meh

## Is it an efficient usage of energy
No

## Why not also use the GPU
My PC only had an integrated one at the time

## Will it damage my processor
Maybe

## Why put this on github
As a reminder of the good old days
