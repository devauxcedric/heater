#include "bor-util.h"

void processHeater(){
        int random;
        while(1)
                random = rand() * 100000;
}

void processSensors(){
        execl("/usr/bin/sensors", "sensors", NULL);
        perror("execl\n");
        exit(1);
}

void processClear(){
        execl("/usr/bin/clear", "clear", NULL);
        perror("execl\n");
        exit(1);
}

void spawn_instance(char arg){
        int forkres = fork();
        if(forkres < 0) {
                perror("fork heater\n");
                exit(1);
        }
        if(forkres == 0) {
                switch (arg) {
                case 'H': processHeater(); break;
                case 'S': processSensors(); break;
                case 'C': processClear(); break;
                }
        }
}

void alarm_handler(){
        alarm(1);
        spawn_instance('S');
        spawn_instance('C');
}

void quit_handler(){
        printf("FIN DU PROG\n");
        exit(0);
}

int main() {

        spawn_instance('H');
        spawn_instance('H');

        alarm(1);
        bor_signal(SIGALRM, alarm_handler, SA_RESTART);
        bor_signal(SIGINT, quit_handler, 0);
        while(1);

        return 0;
}
